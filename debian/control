Source: vulkan-volk
Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>
Section: graphics
Priority: optional
Build-Depends: debhelper-compat (= 13),
               cmake,
               libvulkan-dev (>= 1.3.215)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/xorg-team/vulkan/vulkan-volk/
Vcs-Git: https://salsa.debian.org/xorg-team/vulkan/vulkan-volk.git
Homepage: https://github.com/zeux/volk
Rules-Requires-Root: no

Package: libvulkan-volk-dev
Architecture: all
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends}
Conflicts: libvolk2-dev
# See https://github.com/zeux/volk/issues/66 for name clash
Description: Meta-loader for Vulkan API
 Volk allows you to dynamically load entrypoints required to use Vulkan
 without linking to vulkan-1.dll or statically linking Vulkan loader.
 Additionally, volk simplifies the use of Vulkan extensions by automatically
 loading all associated entrypoints. Finally, volk enables loading Vulkan
 entrypoints directly from the driver which can increase performance by
 skipping loader dispatch overhead.
